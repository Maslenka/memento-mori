/* ===================================================================
 * Sublime - Main JS
 *
 * ------------------------------------------------------------------- */

(function($) {

    "use strict";

    var cfg = {
        scrollDuration : 800, // smoothscroll duration
        mailChimpURL   : 'https://facebook.us8.list-manage.com/subscribe/post?u=cdb7b577e41181934ed6a6a44&amp;id=e6957d85dc'   // mailchimp url
    },

    $WIN = $(window);

    // Add the User Agent to the <html>
    // will be used for IE10 detection (Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0))
    var doc = document.documentElement;
    doc.setAttribute('data-useragent', navigator.userAgent);

    // svg fallback
    if (!Modernizr.svg) {
        $(".header-logo img").attr("src", "images/logo.png");
    }


   /* Preloader
    * -------------------------------------------------- */
    var ssPreloader = function() {

        $("html").addClass('ss-preload');

        $WIN.on('load', function() {

            //force page scroll position to top at page refresh
            $('html, body').animate({ scrollTop: 0 }, 'normal');

            // will first fade out the loading animation
            $("#loader").fadeOut("slow", function() {
                // will fade out the whole DIV that covers the website.
                $("#preloader").delay(300).fadeOut("slow");
            });

            // for hero content animations
            $("html").removeClass('ss-preload');
            $("html").addClass('ss-loaded');

        });
    };


   /* Menu on Scrolldown
    * ------------------------------------------------------ */
    var ssMenuOnScrolldown = function() {

        var menuTrigger = $('.header-menu-toggle');

        $WIN.on('scroll', function() {

            if ($WIN.scrollTop() > 150) {
                menuTrigger.addClass('opaque');
            }
            else {
                menuTrigger.removeClass('opaque');
            }

        });
    };


   /* OffCanvas Menu
    * ------------------------------------------------------ */
    var ssOffCanvas = function() {

        var menuTrigger     = $('.header-menu-toggle'),
            nav             = $('.header-nav'),
            closeButton     = nav.find('.header-nav__close'),
            siteBody        = $('body'),
            mainContents    = $('section, footer');

        // open-close menu by clicking on the menu icon
        menuTrigger.on('click', function(e){
            e.preventDefault();
            siteBody.toggleClass('menu-is-open');
        });

        // close menu by clicking the close button
        closeButton.on('click', function(e){
            e.preventDefault();
            menuTrigger.trigger('click');
        });

        // close menu clicking outside the menu itself
        siteBody.on('click', function(e){
            if( !$(e.target).is('.header-nav, .header-nav__content, .header-menu-toggle, .header-menu-toggle span') ) {
                siteBody.removeClass('menu-is-open');
            }
        });

    };


   /* Masonry
    * ---------------------------------------------------- */
    var ssMasonryFolio = function () {

        var containerBricks = $('.masonry');

        containerBricks.imagesLoaded(function () {
            containerBricks.masonry({
                itemSelector: '.masonry__brick',
                resize: true
            });
        });
    };


   /* photoswipe
    * ----------------------------------------------------- */
    var ssPhotoswipe = function() {
        var items = [],
            $pswp = $('.pswp')[0],
            $folioItems = $('.item-folio');

        // get items
        $folioItems.each( function(i) {

            var $folio = $(this),
                $thumbLink =  $folio.find('.thumb-link'),
                $title = $folio.find('.item-folio__title'),
                $caption = $folio.find('.item-folio__caption'),
                $titleText = '<h4>' + $.trim($title.html()) + '</h4>',
                $captionText = $.trim($caption.html()),
                $href = $thumbLink.attr('href'),
                $size = $thumbLink.data('size').split('x'),
                $width  = $size[0],
                $height = $size[1];

            var item = {
                src  : $href,
                w    : $width,
                h    : $height
            }

            if ($caption.length > 0) {
                item.title = $.trim($titleText + $captionText);
            }

            items.push(item);
        });

        // bind click event
        $folioItems.each(function(i) {

            $(this).on('click', function(e) {
                e.preventDefault();
                var options = {
                    index: i,
                    showHideOpacity: true
                }

                // initialize PhotoSwipe
                var lightBox = new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options);
                lightBox.init();
            });

        });
    };


   /* slick slider
    * ------------------------------------------------------ */
    var ssSlickSlider = function() {

        $('.testimonials__slider').slick({
            arrows: false,
            dots: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            pauseOnFocus: false,
            autoplaySpeed: 1500
        });
    };


   /* Smooth Scrolling
    * ------------------------------------------------------ */
    var ssSmoothScroll = function() {

        $('.smoothscroll').on('click', function (e) {
            var target = this.hash,
            $target    = $(target);

                e.preventDefault();
                e.stopPropagation();

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, cfg.scrollDuration, 'swing').promise().done(function () {

                // check if menu is open
                if ($('body').hasClass('menu-is-open')) {
                    $('.header-menu-toggle').trigger('click');
                }

                window.location.hash = target;
            });
        });

    };


   /* Alert Boxes
    * ------------------------------------------------------ */
    var ssAlertBoxes = function() {

        $('.alert-box').on('click', '.alert-box__close', function() {
            $(this).parent().fadeOut(500);
        });

    };


   /* Animate On Scroll
    * ------------------------------------------------------ */
    var ssAOS = function() {

        AOS.init( {
            offset: 200,
            duration: 600,
            easing: 'ease-in-sine',
            delay: 300,
            once: true,
            disable: 'mobile'
        });

    };


   /* Initialize
    * ------------------------------------------------------ */
    (function clInit() {

        ssPreloader();
        ssMenuOnScrolldown();
        ssOffCanvas();
        ssMasonryFolio();
        ssPhotoswipe();
        ssSlickSlider();
        ssSmoothScroll();
        ssAlertBoxes();
        ssAOS();

    })();

    $.getJSON( "r1.json", function( data ) {
        var items = [];
        $.each( data, function( key, val ) {
            items[key] = val;
        });

        if(items['score'] > 0) {
            var bosses = Math.floor(items['score'] / 750000);
            if (bosses > 12) bosses = 12;
            $('#r1_current_progress').html(bosses + '/10 M');
            $('#r1_current_progress2').html(bosses + '/10 M');
        };
        if(items['world_rank'] > 0) {
            $('#r1_current_progress_world').html(items['world_rank']);
            $("#r1_current_progress_world").removeClass();
            $("#r1_current_progress_world").addClass(my_get_color_class(items['world_rank']));
        };
        if(items['area_rank'] > 0) {
            $('#r1_current_progress_region').html(items['area_rank']);
            $("#r1_current_progress_region").removeClass();
            $("#r1_current_progress_region").addClass(my_get_color_class(items['area_rank']));
        };
        if(items['realm_rank'] > 0) {
            $('#r1_current_progress_realm').html(items['realm_rank']);
            $("#r1_current_progress_realm").removeClass();
            $("#r1_current_progress_realm").addClass(my_get_color_class(items['realm_rank']));
        };
    });


    $.getJSON( "r2.json", function( data ) {
        var items = [];
        $.each( data, function( key, val ) {
            items[key] = val;
        });

        if(items['score'] > 0) {
            var bosses = Math.floor(items['score'] / 750000);
            if (bosses > 12) bosses = 12;
            console.log(items['score']);
            if (bosses == 0) {
                var bosses = Math.floor(items['score'] / 1500);
                if (bosses > 12) bosses = 12;
                $('#r2_current_progress').html( bosses + '/10 HC');
                $('#r2_current_progress2').html( bosses + '/10 HC');
            } else {
                $('#r2_current_progress').html(bosses + '/10 M');
                $('#r2_current_progress2').html(bosses);

            }
        };
        if(items['world_rank'] > 0) {
            $('#r2_current_progress_world').html(items['world_rank']);
            $("#r2_current_progress_world").removeClass();
            $("#r2_current_progress_world").addClass(my_get_color_class(items['world_rank']));
        };
        if(items['area_rank'] > 0) {
            $('#r2_current_progress_region').html(items['area_rank']);
            $("#r2_current_progress_region").removeClass();
            $("#r2_current_progress_region").addClass(my_get_color_class(items['area_rank']));
        };
        if(items['realm_rank'] > 0) {
            $('#r2_current_progress_realm').html(items['realm_rank']);
            $("#r2_current_progress_realm").removeClass();
            $("#r2_current_progress_realm").addClass(my_get_color_class(items['realm_rank']));
        };
    });

    function my_get_color_class(x) {
        if (x < 4) return 'text-artifact';
        if (x < 11) return 'text-legendary';
        if (x < 101) return 'text-epic';
        if (x < 10001) return 'text-rare';
        return 'text-uncommon';
    }

})(jQuery);
